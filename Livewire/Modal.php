<?php

namespace Ksum\Crud\Livewire;

use Livewire\Component;

class Modal extends Component
{
    public $isOpen = false;
    public $form = '';
    public $params = [];
    public $item = null;
    public $width;

    protected $listeners = [
        'showModal' => 'open',
        'closeModal' => 'close'
    ];

    public function open(string $form, array $params = [], $item = null, $width = "lg:max-w-xl"){
        $this->isOpen = true;
        $this->form = $form;
        $this->params = $params;
        $this->item = $item;
        $this->width = $width;
    }

    public function close(){
        $this->isOpen = false;
    }

    public function render()
    {
        return view('ksum::livewire.modal');
    }
}
