<?php

namespace Ksum\Crud\Livewire;

use Ksum\Crud\Traits\ModelTrait;
use Ksum\Crud\Traits\PermissionTrait;
use Livewire\Component;
use Illuminate\Support\Str;
use Intervention\Image\ImageManagerStatic as Image;
use Illuminate\Support\Facades\Storage;
use Jantinnerezo\LivewireAlert\LivewireAlert;

class Form extends Component
{
    use ModelTrait, PermissionTrait, LivewireAlert;

    public $params, $item, $permission, $form, $tabs = [], $currentTab = 0, $tabHeadings=[], $saveButton = "Save", $isModel = true;

    protected function rules(){
        $result = [];
        foreach($this->getForm() as $key => $value){
            if(array_key_exists('validation', $value)){
                $result['item.'.$key] = $value['validation'];
            }
        }
        return $result;
    }

    public function validationAttributes(){
        $result = [];
        foreach($this->getForm() as $key => $value){
            $result['item.'.$key] = strtolower($value['label']);
        }
        return $result;
    } 

    public function getForm()
    {
        if (method_exists($this, 'form')) return $this->form();
        if (property_exists($this, 'form')) return $this->form;

        return [];
    }


    public function save(){
        $perm_slug = is_array($this->item) && array_key_exists('id', $this->item) ? 'edit' : 'add';

        if($this->authorize([$this->makePerm($perm_slug)])===true){

            $this->validate();

            $id = $this->item['id']??null;

            if($id){
                try {
                    $id = decrypt($id);
                } catch (\Illuminate\Contracts\Encryption\DecryptException $e) {
                    $this->alert('danger', 'Invalid');
                    return;
                }
                $this->updateItem($id);
            }
            else{
                $this->createItem();
            }

            $this->closeModal();

            $this->emit('refreshDatatable');

            $this->alert('success', 'Saved', [ 'text' => 'Item saved successfully' ]);
            
        }
    }

    public function updateItem($id){

    }

    public function createItem(){

    }

    public function setAutoParams($model){
        $auto_params = array_keys(array_filter($this->getForm(), function($item){
            return $item['model']??false;
        }));

        foreach($auto_params as $param){
            $model->setAttr($this->item, $param);
        }

        return $model;
    }

    public function updated($propertyName)
    {
        $this->validateOnly($propertyName);
    }

    public function saveFile($file, $folder, $disk="local"){
        $filename = (string) Str::uuid().'.pdf';
        $file->storeAs($folder, $filename, $disk);
        return $filename;
    }

    public function saveImage($image, $folder){
        $img = Image::make($image)->encode();
        $image_name = (string) Str::uuid().'.jpg';

        Storage::put($folder.'/'.$image_name, $img);

        return $image_name;
    }

    public function filterDbData($query, $initial, $model, $key, $value){
        if($initial){
            $role = $model::find($query)->toArray();

            if(is_array($query)){
                $roles = $model::select($key,$value)->whereNotIn($key, $query)->limit(5)->get()->toArray();

                return array_merge($roles, $role);
            }
            else{
                $roles = $model::select($key,$value)->where($key, '<>' ,$query)->limit(5)->get()->toArray();

                array_push($roles, $role);

                return $roles;
            }

            // return is_array($query) ? $role->toArray() : [$role];
        }
        else{
            $role = $model::select($key,$value)->where($value, 'like', '%'.$query.'%')->limit(5)->get()->toArray();
            return $role;
        }
    }

    public function render()
    {
        return view('ksum::livewire.form');
    }
}
