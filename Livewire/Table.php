<?php

namespace Ksum\Crud\Livewire;

use Ksum\Crud\Traits\ActionsTrait;
use Illuminate\Database\Eloquent\Builder;
use Rappasoft\LaravelLivewireTables\DataTableComponent;
use Rappasoft\LaravelLivewireTables\Views\Column;

abstract class Table extends DataTableComponent
{
    use ActionsTrait;
    
    public function columns(): array
    {
      $actions = $this->actions;
      $tbl_columns = $this->table_columns();
      
      array_push(
        $tbl_columns,
        Column::make(__('Actions'))->format(function ($row) use($actions){
          return view('ksum::components.actions', compact('row', 'actions'));
        })
      );

      return $tbl_columns;
    }

    public function table_columns(): array
    {
      return [];
    }

    abstract public function query(): Builder;
}
