<?php

namespace Ksum\Crud\Livewire;

use Ksum\Crud\Traits\ActionsTrait;
use Illuminate\Database\Eloquent\Builder;
use Rappasoft\LaravelLivewireTables\DataTableComponent;
use Rappasoft\LaravelLivewireTables\Views\Column;

abstract class Card extends Table
{
    use ActionsTrait;
    
    public function columns(): array
    {
      $actions = $this->actions;
      $tbl_columns = $this->table_columns();
      
      array_push(
        $tbl_columns,
        Column::make()->format(function ($row) use($actions){
          return view('ksum::components.actions', compact('row', 'actions'));
        })->addClass('justify-center text-center')
      );

      return $tbl_columns;
    }

    public function table_columns(): array
    {
      return [];
    }

    abstract public function query(): Builder;

    /**
     * The view to render each row of the table.
     *
     * @return string
     */
    public function rowView(): string
    {
        return 'livewire-tables::'.config('livewire-tables.theme').'.components.table.row-cards';
    }

    /**
     * @return mixed
     */
    public function render()
    {
        return view('livewire-tables::'.config('livewire-tables.theme').'.datacard')
            ->with([
                'columns' => $this->columns(),
                'rowView' => $this->rowView(),
                'filtersView' => $this->filtersView(),
                'customFilters' => $this->filters(),
                'rows' => $this->rows,
            ]);
    }

}
