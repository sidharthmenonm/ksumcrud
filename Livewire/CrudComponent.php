<?php

namespace Ksum\Crud\Livewire;

use Ksum\Crud\Traits\ModelTrait;
use Ksum\Crud\Traits\PermissionTrait;
use Livewire\Component;
use Intervention\Image\ImageManagerStatic as Image;
use Illuminate\Support\Facades\Storage;

class CrudComponent extends Component
{
    use ModelTrait, PermissionTrait;

    public $title, $subtitle, $table, $model, $form, $edit_form=null, $show_component=null, $permission="crud", $headerbuttons = [];

    public $deleteData = null;

    protected function getListeners()
    {
        return array_merge(
            [
                'form-show' => 'show',
                'form-edit' => 'edit',
                'form-delete' => 'delete',
                'confirmedDelete',
                'cancelledDelete'

            ],
            $this->additionalListeners()
        );
    }

    public function additionalListeners(): array
    {
        return [];
    }

    public function show($id){
        if($this->authorize([$this->makePerm('view')])===true){

            try {
                $item = decrypt($id);
            } catch (\Illuminate\Contracts\Encryption\DecryptException $e) {
                $this->alert('danger', 'Invalid');
                return;
            }

            $this->showItem($item);

        }
    }

    public function showItem($item_id){
        $item = $this->model::findOrFail($item_id);
        $this->openModal($this->show_component, $this->createParams(), $this->showExtra($item));
    }

    public function create(){
        if($this->authorize([$this->makePerm('add')])===true){
            $this->openModal($this->form, $this->createParams(), $this->createExtra());
        }
    }

    public function tableParams(){
        return [];
    }

    public function showExtra($item){
        return $this->editExtra($item);
    }

    public function createExtra(){
        return [];
    }

    public function createParams(){
        return [];
    }

    public function edit($id){
        if($this->authorize([$this->makePerm('edit')])===true){

            try {
                $item_id = decrypt($id);
            } catch (\Illuminate\Contracts\Encryption\DecryptException $e) {
                $this->alert('danger', 'Invalid');
                return;
            }

            $item = $this->model::findOrFail($item_id);

            $this->openModal($this->edit_form??$this->form, $this->createParams(), $this->editExtra($item) );
        }
    }

    public function editExtra($item){
        $item->id = encrypt($item->id); //fix error when calling relations in edit extra.
        return $item->toArray();
    }

    public function delete($id){
        if($this->authorize([$this->makePerm('delete')])===true){

            $this->deleteData = $id;

            $this->confirm('Delete?', [
                "text" => "Are you sure you want to proceed?",
                'onConfirmed' => 'confirmedDelete',
                'onCancelled' => 'cancelledDelete'
            ]);
        }
    }

    public function confirmedDelete(){ 

        try {
            $id = decrypt($this->deleteData);
        } catch (\Illuminate\Contracts\Encryption\DecryptException $e) {
            $this->alert('danger', 'Invalid');
            return;
        }

        $deleteData = $this->model::findOrFail($id);
        $deleteData->delete();

        $this->emit('refreshDatatable');

        $this->alert('success', 'Deleted', [ 'text' => 'Item deleted successfully' ]);
    }

    public function cancelledDelete(){
        $this->reset('deleteData');
    }

    public function getImage($image, $folder, $disk="local"){
        $image = Storage::disk($disk)->get($folder.'/'.$image);
        $image = (string) Image::make($image)->encode('data-url');
        return $image;
    }

    public function render()
    {
        if($this->authorize([$this->makePerm('view')])===true){
            return view('ksum::livewire.crud-component')->extends('layouts.page')->section('page');
        }
    }
}
