<?php

namespace Ksum\Crud\Components;

use Illuminate\View\Component;

class KebabMenu extends Component
{
    public $class, $name, $menu;
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($class, $name, $menu)
    {
        $this->class = implode(" ", $class);
        $this->name = $name;
        $this->menu = $menu;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('ksum::components.kebab-menu');
    }
}
