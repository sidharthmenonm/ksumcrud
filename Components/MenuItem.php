<?php

namespace Ksum\Crud\Components;

use Illuminate\View\Component;

class MenuItem extends Component
{
    public $title, $route, $perms;
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($title, $route, $perms = null)
    {
        $this->title = $title;
        $this->route = $route;
        $this->perms = $perms;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('ksum::components.menu-item');
    }
}
