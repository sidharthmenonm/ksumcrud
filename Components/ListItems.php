<?php

namespace Ksum\Crud\Components;

use Illuminate\View\Component;

class ListItems extends Component
{
    public $type, $list;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($type, $options)
    {
        $this->type = $type;
        $this->list = $options;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('ksum::components.list-items');
    }
}
