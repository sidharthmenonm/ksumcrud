<?php

namespace Ksum\Crud\Components;

use Illuminate\View\Component;

class FormSelect extends Component
{
    public $options, $key, $value, $selected, $keyvalue, $model;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($options, $model, $key="id", $value="name", $selected = null)
    {
        $this->options = $options;
        $this->key = $key;
        $this->value = $value;
        $this->selected = $selected;
        $this->model = $model;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('ksum::components.form-select');
    }
}
