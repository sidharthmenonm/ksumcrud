<?php

namespace Ksum\Crud\Components;

use Illuminate\View\Component;

class FormSearch extends Component
{
    public $options, $selected, $model;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($options, $model, $selected = null)
    {
        $this->options = $options;
        $this->selected = $selected;
        $this->model = $model;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('ksum::components.form-search');
    }
}
