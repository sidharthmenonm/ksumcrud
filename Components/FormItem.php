<?php

namespace Ksum\Crud\Components;

use Illuminate\View\Component;

class FormItem extends Component
{
    public $key, $value, $params;
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($key, $value, $params)
    {
        $this->key = $key;
        $this->value = $value;
        $this->params = $params;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('ksum::components.form-item');
    }
}
