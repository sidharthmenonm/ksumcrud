<?php

namespace Ksum\Crud\Components;

use Illuminate\View\Component;

class PageHeader extends Component
{
    public $title, $subtitle;
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($title, $subtitle)
    {   
        $this->title = $title;
        $this->subtitle = $subtitle;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('ksum::components.page-header');
    }
}
