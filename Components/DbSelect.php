<?php

namespace Ksum\Crud\Components;

use Illuminate\View\Component;

class DbSelect extends Component
{
    public $options, $key, $value, $selected, $keyvalue, $model, $filter, $multiple, $dbmodel;
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($options, $model, $key="id", $value="name", $selected = null, $filter=null, $multiple=false, $dbmodel="")
    {
        $this->options = $options;
        $this->key = $key;
        $this->value = $value;
        $this->selected = $selected;
        $this->model = $model;
        $this->filter = $filter;
        $this->multiple = $multiple;
        $this->dbmodel = $dbmodel;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('ksum::components.db-select');
    }
}
