<?php

namespace Ksum\Crud\Components;

use Illuminate\View\Component;

class SubForm extends Component
{
    public $model, $subform;
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($model, $subform)
    {
        $this->model = $model;
        $this->subform = $subform;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('ksum::components.sub-form');
    }
}
