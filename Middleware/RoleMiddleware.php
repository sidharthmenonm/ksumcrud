<?php

namespace Ksum\Crud\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class RoleMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next, $permission, $guard = null)
    {

        if (Auth::guard($guard)->check()) {

            if($this->_check_role($permission)){
                return $next($request);
            }

        }
        else{
            session()->put('url.intended',url()->current());
            return redirect('/login');
        }

        return redirect('nopermission');
    }

    private function _check_role($permission){

        // dd(Auth::user()->role);
        $permissions = array_column(Auth::user()->role->permissions->toArray(),"name");

        if(in_array($permission,$permissions)){
            return true;
        }
        else{
            return false;
        }
        
    }
}