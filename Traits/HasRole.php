<?php

namespace Ksum\Crud\Traits;

trait HasRole{

  public function role(){
    return $this->belongsTo('Ksum\Crud\Models\Role');
  }

}