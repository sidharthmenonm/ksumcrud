<?php

namespace Ksum\Crud\Traits;

trait TableSearchable{

  public function scopeSearch($query, $term)
  {
      $search = '%'.$term.'%';
      
      return $query->where(function($q) use($search){

          $searchable = $this->searchable;

          $first_param = array_shift($searchable);
          
          $q = $this->_condition($q, $first_param, $search, true);

          foreach($searchable as $search_param){
              $q = $this->_condition($q, $search_param, $search, false);
          }

          return $q;

      });
  }

  private function _condition($q, $param, $search, $first=false){

    $split = explode('.', $param);

    if(count($split)>1){
        //condition on relationship
        $q = $this->_attach_condition_relation($q, $split[0], $split[1], $search, $first);
    }
    else{
        //condition on main
        $q = $this->_attach_condition_main($q, $param, $search, $first);
    }

    return $q;

  }

  private function _attach_condition_relation($q, $relation, $param, $search, $first = false){
    if($first){
        $q = $q->whereHas($relation, function($query) use($param, $search){
            $query->where($param, 'like', $search);
        });
    }
    else{
        $q = $q->orWhereHas($relation, function($query) use($param, $search){
            $query->where($param, 'like', $search);
        });
    }
    return $q;
  }

  private function _attach_condition_main($q, $param, $search, $first=false){
    if($first){
        $q = $q->where($param, 'like', $search);
    }
    else{
        $q = $q->orWhere($param, 'like', $search);
    }
    return $q;
  }

}