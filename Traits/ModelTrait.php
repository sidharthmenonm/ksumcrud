<?php

namespace Ksum\Crud\Traits;

trait ModelTrait{

  protected function openModal(string $form, $params = [], $item = [], $width = "lg:max-w-xl"){
    $this->emitTo('modal', 'showModal', $form, $params, $item, $width);
  }

  public function closeModal(){
    $this->emitTo('modal', 'closeModal');
  }

}