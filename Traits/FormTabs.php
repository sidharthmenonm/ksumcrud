<?php

namespace Ksum\Crud\Traits;

trait FormTabs{

  public function next(){
    foreach($this->tabs[$this->currentTab] as $tabItem){
      $this->validateOnly('item.'.$tabItem);
    }
    $this->currentTab++;
  }

  public function back(){
    $this->currentTab--;
  }
}