<?php

namespace Ksum\Crud\Traits;

trait CrudModel{

  public function setAttr($data, $key, $value = null){
    $this[$key] = $value?($data[$value]??null):($data[$key]??null);
  }

}