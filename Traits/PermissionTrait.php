<?php

namespace Ksum\Crud\Traits;

use Illuminate\Support\Facades\Auth;

trait PermissionTrait{

  public function authorize(array $perms, $guard = null){

    if (Auth::guard($guard)->guest()) {
      return redirect('login');
      die();
    }
    else{
      $permissions = Auth::user()->role->permissions->pluck('name')->toArray();
      // dd($permissions, $perms, array_intersect($perms, $permissions));
      if(empty(array_intersect($perms, $permissions))){
        return redirect('nopermission');
        die();
      }
    }

    return true;

  }

  public function makePerm($slug){
    return $this->permission.":".$slug;
  }

  public function belongsToUser($model, $field = 'user_id'){
    return $model[$field] == Auth::user()->id;
  }

  public function isAdmin(){
    return Auth::user()->role->name == 'super' || Auth::user()->role->name == 'admin';
  }

  public function _decrypt_id($id){
    try {
        return decrypt($id);
    } catch (\Illuminate\Contracts\Encryption\DecryptException $e) {
        return false;
    }
  }

}