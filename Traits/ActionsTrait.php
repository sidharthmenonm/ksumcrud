<?php

namespace Ksum\Crud\Traits;

trait ActionsTrait{
  public $actions = [];

  public function addAction($action){
    array_push($this->actions, $action);
  }

  public function generateActions($perm_slug, $actions=[]){

    if(in_array('show', $actions)){
      $this->addAction([
        'label' => 'Show', 
        'command' => 'form-show', 
        'icon' => 'tabler-eye', 
        'perms' => $perm_slug.":view", 
        'class' => 'bg-green-400 hover:bg-green-500'
      ]);
    }

    if(in_array('edit', $actions)){
      $this->addAction([
        'label' => 'Edit', 
        'command' => 'form-edit', 
        'icon' => 'tabler-edit-circle', 
        'perms' => $perm_slug.":edit", 
        'class' => 'bg-indigo-400 hover:bg-indigo-500'
      ]);
    }

    if(in_array('delete', $actions)){
      $this->addAction([
        'label' => 'Delete', 
        'command' => 'form-delete', 
        'icon' => 'tabler-trash', 
        'perms' => $perm_slug.":delete", 
        'class' => 'bg-red-400 hover:bg-red-500'
      ]);
    }
  }

}