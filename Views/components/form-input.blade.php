<div {{$attributes->only('class')}}>
    <label {{$attributes->only('for')}} class="form-label">
        {{$slot}}
        @if($attributes->get('required'))
            <span class="text-red-400">*</span>
        @endif
    </label>
    @if($attributes->get('helptext'))
        <div class="text-xs text-gray-400 mb-3">{{$attributes->get('helptext')}}</div>
    @endif
    <input {{ $attributes->except(['class','for','model','helptext']) }} wire:model.lazy="item.{{$model}}" class="form-input">
    @error("item.".$model)
        <span class="text-xs text-red-400">{{$message}}</span>
    @enderror
</div>