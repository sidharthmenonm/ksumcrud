@perms($perms)
    <li class="{{ Route::is($route) ? 'active' : '' }} rounded text-sm {{$attributes->get('color')??'text-indigo-200'}} transition duration-700 ease-in-out">
        <a href="{{ Route::has($route) ? route($route) : $route }}" class="py-2 px-3 w-full flex items-center focus:outline-none focus-visible:underline">
            {{$slot}}
            <span>
            {{$title}}
            </span>
        </a>
    </li>
@endperms