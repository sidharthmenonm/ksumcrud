<div class="flex justify-between mb-3">
  <div class="flex flex-col w-full">
    <div class="w-full flex-none text-sm font-medium text-gray-500">
      {{$subtitle}}
    </div>
    <h1 class="flex-auto text-xl font-semibold">
      {{$title}}
    </h1>
  </div>
    {{$slot}}    
</div>