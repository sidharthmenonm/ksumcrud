<div wire:ignore class="mt-5 flex flex-col justify-center  items-center">

    <img :src="getImage()" id="data-crop-{{$attributes->get('name')}}" class="{{$attributes->get('style')}}">

    <button type="button" id="data-button-{{$attributes->get('name')}}" @click="cropResult($dispatch)" class="bg-blue-400 text-white text-sm py-1 px-4 mt-2 rounded shadow hidden z-10">Crop</button>

</div>

{{-- <script src="{{ asset('js/cropper.js') }}"></script> --}}
<script>
    window.imageCropper = function(){
        return {

            imageUrl: @entangle($attributes->wire('model')),
            selected: '',
            target : '',
            cropper : null,
            width: 100,
            height: 100,
            placeholder: @entangle('placeholder'),

            getImage(){
                return this.imageUrl??this.placeholder;
            },

            cropResult(dispatch){
                var canvas = document.createElement('canvas');
                var context = canvas.getContext('2d');

                const image = new Image()
                image.src = this.selected;

                var data = this.cropper.getValue();

                canvas.width = this.width;
                canvas.height = this.height;

                context.drawImage(image, data.x, data.y, data.width, data.height, 0, 0, canvas.width, canvas.height);

                var dataUrl = canvas.toDataURL('image/jpeg');

                // console.log(dataUrl);
                this.cropper.destroy();

                // document.querySelector('#data-crop-'+this.target).src = dataUrl;
                document.querySelector('#data-button-'+this.target).classList.add('hidden');
                // document.querySelector('#data-result-'+this.target).value = dataUrl;

                this.imageUrl = dataUrl;
            },

            fileChosen(e){

                this.fileToDataUrl(e, src => {
                    // this.imageUrl = src
                    this.selected = src
                    this.target = e.target.getAttribute('data-crop');

                    document.querySelector('#data-crop-'+this.target).src = src;

                    this.width = e.target.getAttribute('data-width');
                    this.height = e.target.getAttribute('data-height');
                    var ratio = this.height / this.width;
                
                    this.cropper = new Croppr('#data-crop-'+this.target , {
                        aspectRatio: ratio,
                        startSize: [100, 100, 'px']
                    });

                    document.querySelector('#data-button-'+this.target).classList.remove('hidden');

                });

            },

            fileToDataUrl(event, callback) {
                if (! event.target.files.length) return

                let file = event.target.files[0],
                    reader = new FileReader()

                reader.readAsDataURL(file)
                reader.onload = e => callback(e.target.result)
            },

        }
    }
</script>