<div {{$attributes->only('class')}}>
  <label {{$attributes->only('for')}} class="form-label">
      {{$slot}}
  </label>
  @if($attributes->get('helptext'))
      <div class="text-xs text-gray-400 mb-3">{{$attributes->get('helptext')}}</div>
  @endif

  <div class="bg-gray-100 rounded-md p-5">

      @for ( $i = 0; $i < $this->subforms[$model]; $i++ )

          <div class="p-5 grid grid-cols-1 sm:grid-cols-4 gap-5 mb-5 relative border-b-2 border-gray-200">

              @foreach($subform as $value)

                  @php
                      $option_value = array_key_exists('options', $value) ? ( array_key_exists($value['options'], $params) ? $params[$value['options']] : explode(',', $value['options']) ) :null;
                      $subform_data = array_key_exists('subform', $value) ? $value['subform'] : null;
                  @endphp    

                  <div class="{{$value['class']}}" x-show="{{ $value['visible'] ?? 'true' }}">
                      <x-dynamic-component 
                          component="{{$value['type']}}" 
                          model="{{$model.'.'.$i.'.'.$value['slug']}}" 
                          :options="$option_value" 
                          type="{{$value['attrs']['type']??''}}" 
                          placeholder="{{$value['attrs']['placeholder']??''}}" 
                          helptext="{{$value['attrs']['helptext']??''}}" 
                          text="{{$value['attrs']['text']??''}}" 
                          name="{{$value['slug']}}" 
                          id="id_{{$model}}_{{$i}}_{{$value['slug']}}" 
                          for="{{$value['slug']}}" 
                          key=""
                          required="{{$value['required']??false}}"
                          :subform="$subform_data"
                      >
                      {{$value['label']}}
                      </x-dynamic-component> 
                  </div>

              @endforeach
          
              @if ($i == $this->subforms[$model]-1 )
                  
                  <button class="absolute -top-2 -right-2 focus:outline-none p-1 mr-1 rounded text-white flex items-center bg-red-500" wire:click="removeSubForm('{{$model}}')">
                      <x-tabler-x class="w-4 h-4"></x-tabler-x>
                  </button>

              @endif

          </div>
          
      @endfor

      <button class="save-button" wire:click="addSubForm('{{$model}}')" >
          Add
      </button>
      
  </div>

  @error("item.".$model)
      <span class="text-xs text-red-400">{{$message}}</span>
  @enderror
</div>