<div {{$attributes->only('class')}}>
    <label {{$attributes->only('for')}} class="form-label">
      {{$slot}}
      @if($attributes->get('required'))
        <span class="text-red-400">*</span>
      @endif
    </label>
    <select {{ $attributes->except(['class','for','model']) }} class="form-input" wire:model.lazy="item.{{$model}}">
        <option value="null" readonly>--select--</option>

      @foreach ($options as $item)

        @if($key)
          <option value="{{$item[$key]}}" {{$selected == $item[$key]?'selected':''}}>{{$item[$value]}}</option>              
        @else
          <option value="{{$item}}" {{$selected == $item?'selected':''}}>{{$item}}</option>              
        @endif

      @endforeach
    </select>
    @error("item.".$model)
        <span class="text-xs text-red-400">{{$message}}</span>
    @enderror
</div>