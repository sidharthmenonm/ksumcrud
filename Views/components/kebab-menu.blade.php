<div class="relative {{$class}}" x-data="{ {{$name}}:false}">
    <button class="focus:outline-none px-2" @click="{{$name}} = true">
        <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-dots-vertical {{$attributes->get('color')??'text-indigo-300'}}" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
        <path stroke="none" d="M0 0h24v24H0z" fill="none"></path>
        <circle cx="12" cy="12" r="1"></circle>
        <circle cx="12" cy="19" r="1"></circle>
        <circle cx="12" cy="5" r="1"></circle>
        </svg>
    </button>
    <div x-show="{{$name}}" @click.away="{{$name}} = false" class="origin-top-right absolute bottom-10 right-0 mt-2 w-56 rounded-md shadow-lg bg-white ring-1 ring-black ring-opacity-5 focus:outline-none" role="menu" aria-orientation="vertical" aria-labelledby="menu-button" tabindex="-1">
        <div class="py-1" role="none">
        @foreach ($menu as $key=>$value)
            <a href="{{$value}}" class="text-gray-700 block px-4 py-2 text-sm hover:bg-gray-100 hover:text-gray-900" role="menuitem" tabindex="-1" >
                {{$key}}
            </a>
        @endforeach
        
        </div>
    </div>
</div>