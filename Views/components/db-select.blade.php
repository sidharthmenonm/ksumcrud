<div class="{{$attributes->get('class')}} relative" x-data="selectComponent{{$model}}()" x-init="init($wire)">
  <label {{$attributes->only('for')}} class="form-label">
    {{$slot}}
    @if($attributes->get('required'))
      <span class="text-red-400">*</span>
    @endif
  </label>
  @if($attributes->get('helptext'))
      <div class="text-xs text-gray-400 mb-3">{{$attributes->get('helptext')}}</div>
  @endif
  <div x-on:click="openSelect()" class="form-input bg-white shadow p-2 flex items-center justify-between cursor-pointer">
    <div class="truncate" x-text="selectedValue||'Select'"></div>
    <svg x-show="loading" class="animate-spin h-5 w-5 text-green-400" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24">
      <circle class="opacity-25" cx="12" cy="12" r="10" stroke="currentColor" stroke-width="4"></circle>
      <path class="opacity-75" fill="currentColor" d="M4 12a8 8 0 018-8V0C5.373 0 0 5.373 0 12h4zm2 5.291A7.962 7.962 0 014 12H0c0 3.042 1.135 5.824 3 7.938l3-2.647z"></path>
    </svg>
  </div>
  <div class=" bg-white shadow w-full left-0 top-full mt-1 text-xs cursor-pointer" x-show="open" x-on:click.away="open = !open">
    <div class="p-2 border-b">
      <input type="search" x-ref="search" x-model.debounce.300ms="search" placeholder="Type to search" class="appearance-none rounded w-full py-2 px-3 text-gray-700 border border-gray-200 shadow-sm leading-tight focus:outline-none">
    </div>
    <div class="">
      <template x-for="(item, index) in options" :key="index">
        <div class="p-3 bg-white hover:bg-gray-100 border-b cursor-pointer relative" x-on:click="selectItem(item)" x-bind:class="{'bg-green-200':isSelected(item)}" >
          <span x-text="key?item[value]:item"></span>
          <svg x-show="!isSelected(item)" xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-square opacity-50 absolute right-2 top-1/2 transform -translate-y-1/2" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
            <path stroke="none" d="M0 0h24v24H0z" fill="none"></path>
            <rect x="4" y="4" width="16" height="16" rx="2"></rect>
         </svg>
         <svg x-show="isSelected(item)" xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-checkbox opacity-50 absolute right-2 top-1/2 transform -translate-y-1/2" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
          <path stroke="none" d="M0 0h24v24H0z" fill="none"></path>
          <polyline points="9 11 12 14 20 6"></polyline>
          <path d="M20 12v6a2 2 0 0 1 -2 2h-12a2 2 0 0 1 -2 -2v-12a2 2 0 0 1 2 -2h9"></path>
        </svg>
        </div>
      </template>
      <template x-if="options.length==0">
        <div class="p-3">No items</div>
      </template>
    </div>
  </div>
  @error("item.".$model)
      <span class="text-xs text-red-400">{{$message}}</span>
  @enderror
</div>

<script>
window.selectComponent{{$model}} = function(){
  return {
    open: false,
    selected: @entangle('item.'.$model),
    selectedValue: '',
    key: '{{$key}}',
    value: '{{$value}}',
    filter: '{{$filter}}',
    multiple: "{{$multiple}}",
    dbmodel: @json($dbmodel),
    options: @json($options)||[],
    data: @json($options)||[],
    search: '',
    loading: false,
    selectItem(item){
      if(this.multiple){
        //check if item already selected
        if(this.selected.includes( this.key ? item[this.key] : item )){
          //deselect selected item
          this.selected = this.selected.filter(i => i != ( this.key ? item[this.key] : item ) );

          value_split = this.splitSelectedValues();
          value_split = value_split.filter(i => i != ( this.key ? item[this.value] : item ) );
          this.selectedValue = value_split.join(', ');
        }
        else{
          this.selected.push(( this.key ? item[this.key] : item ));

          value_split = this.splitSelectedValues();
          value_split.push(( this.key ? item[this.value] : item ));
          this.selectedValue = value_split.join(', ');
        }
      }
      else{
        this.selected = ( this.key ? item[this.key] : item );
        this.selectedValue = ( this.key ? item[this.value] : item );
      }

      this.open = false;
    },
    splitSelectedValues(item){
      return this.selectedValue.split(',').map(i => i.trim()).filter(i => i.length);
    },
    isSelected(item){
      if(this.multiple){
        return this.selected.includes(( this.key ? item[this.key] : item ));
      }
      else{
        return ( this.key ? item[this.key] : item ) == this.selected;
      }
    },
    openSelect(){
      this.open = true;
      this.$nextTick(() => {
        this.$refs.search.focus({
            preventScroll: true,
        });
      });
    },
    searchFilter($wire, value, initial = false){
      this.loading = true;
      $wire[this.filter](value, initial, this.dbmodel, this.key, this.value).then(data => {
        this.data = data;
        this.options = data;
        if(initial){
          var sel = data.filter(i => this.selected.includes(i[this.key]))
          var sel_value = sel.map(i => i[this.value]);
          this.selectedValue = sel_value.join(', ');
        }
        this.loading = false;
      }).catch(err => {
        console.log(err);
        this.loading = false;
      });
    },
    init($wire){
      if(this.selected){
        if(this.filter){
          this.searchFilter($wire, this.selected, true);
        }
        else{
          var selected = this.options.filter(item => ( this.key ? item[this.key] : item ) == this.selected);
          if(selected.length){
            this.selectedValue = this.key ? selected[0][this.value] : selected[0];
          }
        }
      }
      else{
        if(this.filter){
          this.searchFilter($wire, '');
        }
      }

      this.$watch('search', value => {
        
        if(!this.open){
          return this.options = this.data;
        }

        if(this.filter){

          this.searchFilter($wire, value);

          return
        }

        if (! value) {
          return this.options = this.data;
        }

        this.options = this.data.filter(item => ( this.key ? item[this.value] : item ).toLowerCase().includes(value.toLowerCase()));

      });
    }
  }
}
</script>
