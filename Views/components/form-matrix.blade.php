<div class="grid grid-cols-6 gap-5 col-span-6">
    <label {{$attributes->only('for')}} class="form-label col-span-6">
        {{$slot}}
        @if($attributes->get('required'))
            <span class="text-red-400">*</span>
        @endif
    </label>
    @if($attributes->get('helptext'))
        <div class="text-xs text-gray-400 mb-3">{{$attributes->get('helptext')}}</div>
    @endif
    @foreach ($options as $index=>$item)

        <div {{$attributes->only('class')}}>
            <label class="flex items-center">
                <input
                    {{ $attributes->except(['class','for','model']) }} 
                    class="form-checkbox h-4 w-4 text-gray-600" 
                    value="{{$key?$item[$key]:$item}}"
                    wire:model.lazy="item.{{$model}}.{{$key?$item[$key]:$item}}"
                >
                <span class="ml-2 text-gray-700 text-sm">
                    {{$key?$item[$value]:$item}}
                </span>
            </label>
        </div>

    @endforeach

    @error("item.".$model)
        <span class="text-xs text-red-400">{{$message}}</span>
    @enderror
</div>