<div {{ $attributes->only('class') }} >
  <label {{$attributes->only('for')}} class="form-label">
      {{$slot}}
      @if($attributes->get('required'))
      <span class="text-red-400">*</span>
      @endif
  </label>
  @if($attributes->get('helptext'))
    <div class="text-xs text-gray-400 mb-3">{{$attributes->get('helptext')}}</div>
  @endif
  <div 
      wire:ignore 
      x-data="filePondComponent_{{$model}}()" 
      x-on:file-pond-clear.window="clearPond($wire)"
      x-init="init()"
      >

      <input x-ref="fileinput"
          type="file"
          style="display: none;"
          {{ $attributes->except('class') }}
      />

  </div>
  @error("item.".$model)
      <span class="text-xs text-red-400">{{$message}}</span>
  @enderror
</div>

<script>
  window.filePondComponent_{{$model}} = function(){
    return {
      pond:null,

      init(){
          this.pond = FilePond.create(this.$refs.fileinput,{
              allowMultiple: {{ $attributes->get('multiple')??'false' }},
              allowDrop: {{ $attributes->get('drop')??'true' }},
              maxFiles: {{ $attributes->get('max-files')??1 }},
              name: '{{ $attributes->get('name') }}',
              server: {
                  process: (fieldName, file, metadata, load, error, progress, abort, transfer, options) => {
                      @this.upload('item.{{ $model }}', file, load, error, progress);
                  },
                  revert: (filename, load) => {
                      @this.removeUpload('item.{{ $model }}', filename, load);
                  },
              }
          });
      },
      clearPond($wire){

      }

    }
  }
</script>