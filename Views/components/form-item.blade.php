<?php
    $option_value = array_key_exists('options', $value) ? ( array_key_exists($value['options'], $params) ? $params[$value['options']] : explode(',', $value['options']) ) :null;
    $subform_data = array_key_exists('subform', $value) ? $value['subform'] : null;
?>

@if(array_key_exists('visible', $value))
    <div class="{{$value['class']}}" x-data="{ item: @entangle('item') }" x-show="{{$value['visible']}}">
            <x-dynamic-component 
                component="{{$value['type']}}" 
                model="{{$key}}" 
                {{-- class="{{$value['class']}}"   --}}
                :options="$option_value" 
                type="{{$value['attrs']['type']??''}}" 
                name="{{$key}}" 
                id="{{$key}}" 
                for="{{$key}}" 
                required="{{$value['required']??false}}"
                :subform="$subform_data"
                {{ $attributes->merge($value['attrs']) }}
            >
            {{$value['label']}}
            </x-dynamic-component>
    </div>
@else
    <x-dynamic-component 
        component="{{$value['type']}}" 
        model="{{$key}}" 
        class="{{$value['class']}}"  
        :options="$option_value" 
        type="{{$value['attrs']['type']??''}}" 
        name="{{$key}}" 
        id="{{$key}}" 
        for="{{$key}}" 
        required="{{$value['required']??false}}"
        {{ $attributes->merge($value['attrs']) }}
    >
    {{$value['label']}}
    </x-dynamic-component>
@endif
