<div {{$attributes->only('class')}}>
    <label {{$attributes->only('for')}} class="form-label">
        {{$slot}}
    </label>

    @if($attributes->get('helptext'))
        <div class="text-xs text-gray-400 mb-3">{{$attributes->get('helptext')}}</div>
    @endif

    @if($attributes->get('type') == "file")
        @if(Arr::get($this->item, $model))
            <a target="_blank" href="{{URL::signedRoute($attributes->get('route'), [$attributes->get('folder'), Arr::get($this->item, $model) ])}}" class="close-button flex">
                <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-download" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
                    <path stroke="none" d="M0 0h24v24H0z" fill="none"></path>
                    <path d="M4 17v2a2 2 0 0 0 2 2h12a2 2 0 0 0 2 -2v-2"></path>
                    <polyline points="7 11 12 16 17 11"></polyline>
                    <line x1="12" y1="4" x2="12" y2="16"></line>
                </svg>
                Download
            </a>
        @endif
    @elseif($attributes->get('type') == "button")
        <button wire:click.prevent="{{$attributes->get('command')}}" class="{{$attributes->get('style')}}">{{$attributes->get('text')}}</button>
    @elseif($attributes->get('type') == "date")
        <div class="form-static">
            {{ Carbon::parse(Arr::get($this->item, $model))->format($attributes->get('format')) }}
        </div>
    @elseif($attributes->get('type') == "image")
        <img src="{{Arr::get($this->item, $model)}}" class="{{$attributes->get('style')}}"/>
    @else
        <div class="form-static text-sm">
            {{Arr::get($this->item, $model)}}
        </div>
    @endif
    {{-- <input {{ $attributes->except(['class','for','model','helptext']) }} wire:model.lazy="item.{{$model}}" readonly disabled class="form-input"> --}}

</div>