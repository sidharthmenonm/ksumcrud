<div {{$attributes->only('class')}}>
    <label {{$attributes->only('for')}} class="form-label">{{$slot}}</label>
    <div class="text-xs text-gray-400" {{ $attributes->except(['class','for','model']) }}>
        {{$text}}
    </div>
</div>