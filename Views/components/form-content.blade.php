<div {{$attributes}}>
    <label class="form-label">{{$attributes->get('label')}}</label>
    <div class="text-sm">
        {{$slot}}
    </div>
</div>