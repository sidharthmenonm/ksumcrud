<div {{$attributes->only('class')}}>
    <label {{$attributes->only('for')}} class="form-label">
        {{$slot}}
        @if($attributes->get('required'))
            <span class="text-red-400">*</span>
        @endif
    </label>
    
    <div x-data="{ content: @entangle('item.'.$model).defer }">
        <input 
            {{ $attributes->except(['class','for','model', 'style', 'type']) }} 
            id="{{ $model }}" 
            type="hidden" 
            >
        
        <div wire:ignore >
            <trix-editor input="{{$model}}" x-model.debounce.300ms="content"></trix-editor>
        </div>
    </div>

    @error("item.".$model)
        <span class="text-xs text-red-400">{{$message}}</span>
    @enderror
</div>