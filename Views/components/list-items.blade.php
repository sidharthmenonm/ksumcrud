<div {{$attributes->only('class')}}>
    <label {{$attributes->only('for')}} class="form-label">{{$slot}}</label>
    <div class="text-sm text-gray-400" {{ $attributes->except(['class','for','model']) }}>
        <ol class="ml-5 text-justify {{$type}}">
            @foreach ($list as $listitem)
                <li class="mb-2">{{$listitem}}</li>
            @endforeach
        </ol>
    </div>
</div>