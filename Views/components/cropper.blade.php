<div {{$attributes->only('class')}}>
    <label {{$attributes->only('for')}} class="form-label">
        {{$slot}}
        @if($attributes->get('required'))
            <span class="text-red-400">*</span>
        @endif
    </label>
    @if($attributes->get('helptext'))
        <div class="text-xs text-gray-400 mb-3">{{$attributes->get('helptext')}}</div>
    @endif
    
    <div x-data="imageCropper()" class="relative">
        
        <x-ksum-cropper-box wire:model.debounce="item.{{$model}}" {{ $attributes->except(['class','for','type', 'helptext']) }}></x-ksum-cropper-box>
        
        <div wire:ignore >
            <input type="file" class="absolute cursor-pointer appearance-none opacity-0 w-full h-full top-0" accept="image/*" data-crop="{{$model}}" @change.prevent="fileChosen" {{ $attributes->except(['class','for','model','type']) }} >
        </div>

    </div>

    @error("item.".$model)
        <span class="text-xs text-red-400">{{$message}}</span>
    @enderror
</div>


    
