<div class="flex">
  @foreach ($actions as $button)
      @perms($button['perms'])
          <button wire:click="$emit('{{$button['command']}}', '{{encrypt($row->id)}}')" class="focus:outline-none p-1 mr-1 rounded text-white flex items-center {{$button['class']??''}}">
              <x-dynamic-component :component="$button['icon']" class="w-4 h-4"></x-dynamic-component>
              <span class="md:hidden">
                {{$button['label']}}
              </span>
          </button>
      @endperms
  @endforeach

</div>