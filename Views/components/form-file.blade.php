<div {{$attributes->only('class')}}
    x-data="{ isUploading: false, progress: 0 }"
    x-on:livewire-upload-start="isUploading = true"
    x-on:livewire-upload-finish="isUploading = false"
    x-on:livewire-upload-error="isUploading = false"
    x-on:livewire-upload-progress="progress = $event.detail.progress"
    >
    <label {{$attributes->only('for')}} class="form-label">
        {{$slot}}
        @if($attributes->get('required'))
            <span class="text-red-400">*</span>
        @endif
    </label>
    @if($attributes->get('helptext'))
        <div class="text-xs text-gray-400 mb-3">{{$attributes->get('helptext')}}</div>
    @endif
    <input {{ $attributes->except(['class','for','model','type', 'helptext']) }} type="file" wire:model.lazy="item.{{$model}}" class="form-file">
    <div x-show="isUploading">
        <progress max="100" x-bind:value="progress"></progress>
    </div>
    @error("item.".$model)
        <span class="text-xs text-red-400">{{$message}}</span>
    @enderror
</div>