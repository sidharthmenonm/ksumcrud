<div {{$attributes->only('class')}}>
    <label {{$attributes->only('for')}} class="form-label">
      {{$slot}}
      @if($attributes->get('required'))
        <span class="text-red-400">*</span>
      @endif
    </label>
    
    <x-ksum-select-search :data="$options" wire:model.lazy="item.{{$model}}" {{ $attributes->except(['class','for','model']) }} class="form-input"/>

    @error("item.".$model)
        <span class="text-xs text-red-400">{{$message}}</span>
    @enderror
</div>