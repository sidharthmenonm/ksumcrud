<x-ksum-page-header title="{{$title}}" subtitle="{{$subtitle}}">
    <div class="flex w-full justify-end items-center">
        @include('ksum::components.loading')

        @foreach ($headerbuttons as $button)
            @perms($button['perms'])
                <button wire:click="{{$button['command']}}()" class="focus:outline-none text-white py-2 px-3 ml-3 text-sm flex items-center rounded {{$button['class']??''}}">
                    <x-dynamic-component :component="$button['icon']" class="w-5 h-5 mr-1"></x-dynamic-component>
                    {{$button['label']}}
                </button>
            @endperms
        @endforeach

        @perms($permission.':add')
            <button wire:click="create()" class="bg-blue-500 hover:bg-blue-600 focus:outline-none text-white py-2 px-3 text-sm flex items-center rounded">
                <x-tabler-plus class="w-5 h-5 mr-1"></x-tabler-plus>
                Create New
            </button>
        @endperms

    </div>
</x-ksum-page-header>
@livewire($table, $this->tableParams())

@livewire('modal')