<form>
  @if(count($tabs)>0)

    <div class="px-6 pb-4 pt-5 bg-gray-200 border-b-2 border-gray-300">
      
      <div class="uppercase tracking-wide text-xs font-bold text-gray-500 mb-1 leading-tight">
        Step {{$currentTab+1}} of {{count($tabs)}}
      </div>

      <div class="flex flex-col md:flex-row md:items-center md:justify-between">
        <div class="flex-1">
          <div class="text-lg font-bold text-gray-700 leading-tight">
            {{$tabHeadings[$currentTab]}}
          </div>
        </div>

        <div class="flex items-center md:w-1/4">
          <?php
            $percent = floor((($currentTab+1)/(count($tabs)))*100) ."%"
          ?>
          <div class="w-full bg-white rounded-full mr-2">
            <div class="rounded-full bg-green-500 text-xs leading-none h-2 text-center text-white" style="width: {{$percent}};"></div>
          </div>
          <div class="text-xs w-10 text-gray-600">{{$percent}}</div>
        </div>
      </div>
    </div>

    
      @foreach ($tabs as $index => $tab)
        
        <div class="form {{$index == $currentTab?'grid':'hidden'}}">

          @foreach ($this->getForm() as $key => $value)

            @if (in_array($key, $tab))
              
              <x-ksum-form-item :key="$key" :value="$value" :params="$params"></x-ksum-form-item>
              
            @endif
              
          @endforeach

        </div>

      @endforeach
    

  @else
    <div class="form grid">
      @foreach ($this->getForm() as $key => $value)

        <x-ksum-form-item :key="$key" :value="$value" :params="$params"></x-ksum-form-item>
          
      @endforeach
    </div>
  @endif

  <div class="form-footer">
    @if(count($tabs)>0)
      @if ($currentTab == count($tabs)-1)
        <button class="save-button" wire:click.prevent="save()" wire:loading.attr="disabled">{{$saveButton}}</button>  
      @else
        <button class="save-button" wire:click.prevent="next()" wire:loading.attr="disabled">Next</button> 
      @endif
    @else
      @if($saveButton)
        <button class="save-button" wire:click.prevent="save()" wire:loading.attr="disabled">{{$saveButton}}</button>  
      @endif
    @endif

    @if($currentTab > 0)
      <button class="close-button" wire:click.prevent="back()" wire:loading.attr="disabled">Back</button>  
    @endif

    @if($currentTab == 0 && count($tabs) > 0)
      <button class="close-button opacity-50 select-none" disabled wire:click.prevent>Back</button>  
    @endif

    @if($isModel)
      <button class="close-button" wire:click.prevent="closeModal()" wire:loading.attr="disabled">Close</button>
    @endif

    <div class="block">
      @include('ksum::components.loading')
    </div>
  </div>
</form>