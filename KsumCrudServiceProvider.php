<?php

namespace Ksum\Crud;

use Illuminate\Support\ServiceProvider;
use Ksum\Crud\Components\KebabMenu;
use Ksum\Crud\Components\MenuItem;
use Ksum\Crud\Components\PageHeader;
use Illuminate\Support\Facades\Blade;
use Ksum\Crud\Components\Cropper;
use Ksum\Crud\Components\CropperBox;
use Ksum\Crud\Components\DbSelect;
use Ksum\Crud\Components\FilePond;
use Ksum\Crud\Components\FormContent;
use Ksum\Crud\Components\FormFile;
use Ksum\Crud\Components\FormInput;
use Ksum\Crud\Components\FormItem;
use Ksum\Crud\Components\FormMatrix;
use Ksum\Crud\Components\FormSearch;
use Ksum\Crud\Components\FormSelect;
use Ksum\Crud\Components\FormStatic;
use Ksum\Crud\Components\FormTextarea;
use Ksum\Crud\Components\FormTrix;
use Ksum\Crud\Components\FormWysiwyg;
use Ksum\Crud\Components\Heading;
use Ksum\Crud\Components\ListItems;
use Ksum\Crud\Components\SelectSearch;
use Ksum\Crud\Components\SubForm;
use Ksum\Crud\Livewire\Card;
use Ksum\Crud\Livewire\CrudComponent;
use Ksum\Crud\Livewire\Form;
use Ksum\Crud\Livewire\Modal;
use Ksum\Crud\Livewire\Table;
use Livewire;

class KsumCrudServiceProvider extends ServiceProvider{

  public function boot()
  {

    $this->loadViewsFrom(__DIR__.'/Views', 'ksum');

    $this->publishes([
      __DIR__.'/Views' => resource_path('views/vendor/ksum'),
    ]);

    $this->loadViewComponentsAs('ksum', [
      KebabMenu::class,
      MenuItem::class,
      PageHeader::class,
      Cropper::class,
      CropperBox::class,
      FormContent::class,
      FormInput::class,
      FormItem::class,
      FormMatrix::class,
      FormSelect::class,
      FormTextarea::class,
      Heading::class,
      ListItems::class,
      FormTrix::class,
      FormSearch::class,
      SelectSearch::class,
      FormFile::class,
      FormStatic::class,
      DbSelect::class,
      FilePond::class,
      SubForm::class,
    ]);

    Livewire::component('crud-component', CrudComponent::class);
    Livewire::component('form', Form::class);
    Livewire::component('modal', Modal::class);
    Livewire::component('table', Table::class);
    Livewire::component('card', Card::class);

    $this->loadMigrationsFrom(__DIR__.'/Migrations');

    Blade::if('perms', function($exp){
      if($exp){
          if(!auth()->user()) return;
          
          $req = explode('|', $exp);
          $permissions = array_column(auth()->user()->role->permissions->toArray(),"name");

          return !empty(array_intersect($req, $permissions));
      }
      else{
          return true;
      }
    });
  }
}