<?php

namespace Ksum\Crud\Models;

use Ksum\Crud\Traits\UsesUuid;
use Illuminate\Database\Eloquent\Model;

class Permission extends Model
{
  use UsesUuid;

  protected $fillable = ['name', 'type'];

}