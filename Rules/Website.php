<?php

namespace Ksum\Crud\Rules;

use Illuminate\Contracts\Validation\Rule;

class Website implements Rule
{
    public $type;
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($type)
    {
        $this->type = $type;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if($this->type == 'linkedin')
        {
            $patern = "/^https:\/\/(?:[a-z]{2,3}\.)linkedin\.com\/.*$/";
        }
        elseif($this->type == 'facebook'){
            $patern = "/^(?:https?:\/\/)?(?:www\.|m\.|mobile\.|touch\.|mbasic\.)?(?:facebook\.com|fb(?:\.me|\.com))\/(?!$)(?:(?:\w)*#!\/)?(?:pages\/|pg\/)?(?:photo\.php\?fbid=)?(?:[\w\-]*\/)*?(?:\/)?(?:profile\.php\?id=)?([^\/?&\s]*)(?:\/|&|\?)?.*$/";
        }
        elseif($this->type == 'youtube'){
            $patern = "/^((?:https?:)?\/\/)?((?:www|m)\.)?((?:youtube\.com|youtu.be))(\/(?:[\w\-]+\?v=|embed\/|v\/)?)([\w\-]+)(\S+)?$/";
        }
        else{
            return false;
        }

        return preg_match($patern, $value);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Invalid '.$this->type.' URL';
    }
}
