<?php

namespace Ksum\Crud\Rules;

use Illuminate\Contracts\Validation\Rule;

class minWords implements Rule
{
    public $count;
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($count)
    {
        $this->count = $count;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $words = preg_split( '@\s+@i', trim( $value ) );
        if ( count( $words ) >= $this->count ) {
            return true;
        }
        return false;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return __('validation.minwords', ['minwords' => $this->count]);
    }
}
